import React, { Component } from "react";
import { Redirect, Route, Switch } from "react-router";
import FirstPage from "./Task";
import Second from "./SecPage";
import Result from "./ResultPage";

class Main extends Component {
  state = {};
  render() {
    return (
      <div className="container-fluid">
        <Switch>
          <Route path="/report" component={Result}></Route>
          <Route path="/start" component={FirstPage}></Route>
          <Route path="/quizPage" component={Second}></Route>
          <Redirect to="/start"></Redirect>
        </Switch>
      </div>
    );
  }
}

export default Main;
