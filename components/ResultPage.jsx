import React, { Component } from "react";
import axios from "axios";

class Result extends Component {
  state = {};
  constructor(props) {
    super(props);

    let name = ""; // value[0].name;
    let id = "";
    let wrong = ""; // value[0].wrong;
    let time = "";
    let attempt = ""; // value[0].attempt;
    this.state = { name, wrong, attempt, time };
  }
  async componentDidMount() {
    let { data: data } = await axios.get(
      "https://guarded-crag-86799.herokuapp.com/getPlayers"
    );
    if (data) {
      let attempt = this.props.location.state.sel.length;
      if (data[data.length - 1].name === this.props.location.state.name) {
        this.setState({
          name: data[data.length - 1].name,
          id: data[data.length - 1].id,
          wrong: +data[data.length - 1].wrong,
          attempt: attempt,
          time: data[data.length - 1].time,
        });
      }
    }
  }
  handleQuit = () => {
    this.props.history.push({
      pathname: "/startPage",
      state: "",
    });
  };
  hanldePlay = () => {
    let { name, value, id, time } = this.state;
    console.log(value);
    let js = [name, id ? id : "", time];
    this.props.history.push({
      pathname: "/QuizPage",
      state: js,
    });
  };
  render() {
    return (
      <div className="container">
        <div className="row mt-5 p-2 text-center">
          <div className="col-12">
            <span
              style={{
                fontWeight: "500",
                color: "orangered",
                fontSize: "40px",
              }}
            >
              Welcome to the Report View
            </span>
          </div>
        </div>
        <div className="row col-md-11 col-12  p-3 mt-5  ml-2 mr-3 ">
          <div className="col-sm-4 ">
            <span className="border row bg-warning p-2 text-white">
              Name of the player
            </span>
            <span
              className="row text-center  border p-2"
              style={{ background: "lightGray" }}
            >
              {this.state.name}
            </span>
          </div>

          <div className="col-sm-4">
            <span className="border text-center row bg-warning p-2 text-white">
              Wrong Answers
            </span>
            <span
              className="text-center row  border p-2"
              style={{ background: "lightGray" }}
            >
              {this.state.wrong}
            </span>
          </div>

          <div className="col-sm-4">
            <span className="border text-center row bg-warning p-2 text-white">
              Total Questions
            </span>
            <span
              className="text-center border row p-2"
              style={{ background: "lightGray" }}
            >
              {this.state.attempt}
            </span>
          </div>
        </div>

        <div className="row mt-5 ">
          <div className="col-6 text-right">
            <button
              className="btn btn-outline-success"
              onClick={() => this.hanldePlay()}
            >
              Replay
            </button>
          </div>
          <div className="col-6">
            <button
              className="btn btn-outline-danger"
              onClick={() => this.handleQuit()}
            >
              Quit Quiz
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Result;
