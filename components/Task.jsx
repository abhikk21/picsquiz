import React, { Component } from "react";

class FirstPage extends Component {
  state = { name: "" };
  handleChange = (e) => {
    this.setState({ name: e.currentTarget.value });
  };

  handleSubmit = () => {
    let { name } = this.state;
    let id = "";
    console.log("name", name);
    let val = [name, id];
    this.props.history.replace({
      pathname: "/QuizPage",
      state: val,
    });
  };
  render() {
    return (
      <div className="row mt-5 p-5" style={{ background: "#FAC1D9	" }}>
        <div className="container box mt-5 mb-5">
          <div class="row" style={{ marginTop: "12%" }}>
            <div className="col-md-11 col-12 text-center">
              <strong style={{ fontSize: "42px", color: "red" }}>
                Welcome to Pics Quiz
              </strong>
            </div>
            <div className="col-12 p-4">
              <div className="form-group row mt-4">
                <input
                  type="text"
                  name="name"
                  onChange={this.handleChange}
                  placeholder="Enter Your name"
                  value={this.state.name}
                  className="form-control p-4"
                ></input>
              </div>
            </div>
            <div className="col-12">
              <div className="row mt-5 mb-4">
                <div className="col-12 text-center">
                  <button
                    className={
                      this.state.name
                        ? "btn btn-warning"
                        : "btn btn-warning disabled"
                    }
                    onClick={() => this.handleSubmit()}
                  >
                    {this.state.name ? "Start" : "Enter name to Start"}
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default FirstPage;
