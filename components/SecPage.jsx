import axios from "axios";
import React, { Component } from "react";
import { Redirect } from "react-router";
import "./mcq.css";

class Second extends Component {
  state = {};
  constructor(props) {
    super(props);
    let months = [
      "January",
      "Feb",
      "March",
      "April",
      "May",
      "June",
      "July",
      "Aug",
      "Sept",
      "Oct",
      "Nov",
      "Dec",
    ];
    let name = this.props.location.state[0];
    console.log("na,e", name);
    this.state = {
      name,
      curr: 0,
      limit: 4,
      sel: [],
      wrong: [],
      counter: [],
      months,
    };
  }
  async componentDidMount() {
    try {
      let { data: resp } = await axios.get(
        "https://guarded-crag-86799.herokuapp.com/data"
      );
      /*  let filtdata = resp.map((vl, ind) =>
        ind <= 4 ? resp[Math.floor(Math.random() * resp.length)] : ""
      );*/
      let newfilt = [];
      let arr = [];
      while (arr.length < 5) {
        var r = Math.floor(Math.random() * resp.length - 1) + 1;
        if (arr.indexOf(r) === -1) {
          arr.push(r);
          newfilt.push(resp[r]);
        }
      }

      let newdata = newfilt;
      console.log("filtdata", newdata);

      this.setState({ data: resp, newdata });
    } catch (ex) {
      if (ex.response && ex.response.status == 401) {
        alert("Bad Status");
      }
    }
  }
  handlePage = (ind) => {
    let { curr, sel, wrong } = this.state;
    if (sel[curr] === undefined) {
      return alert("Select the option to continue...");
    }
    if (wrong[curr] !== 0) {
      return alert("Wrong answer try again");
    }
    if (sel[curr] !== undefined) curr = curr + ind;
    this.setState({ curr });
  };
  hanldeSel = (ind) => {
    let { sel, curr, newdata, wrong, counter } = this.state;
    console.log("sel,", sel[curr]);
    sel[curr] !== undefined ? (sel[curr] = ind) : sel.push(ind);

    if (newdata[curr].answer == sel[curr]) {
      wrong[curr] = 0;
    } else {
      wrong[curr] = 1;
      counter[curr] = 1;
      alert("Wrong answer try again");
    }

    this.setState({ sel, wrong, counter });
  };
  handleQuit = () => {
    let { sel } = this.state;
    this.props.history.push({
      pathname: "/startPage",
      state: "",
    });
  };
  handleSubmit = async () => {
    let { name, wrong, sel, months, curr, counter } = this.state;
    console.log("counter", counter);

    if (sel[curr] === undefined) {
      return alert("Select the option to Submit...");
    }
    if (wrong[curr] !== 0) {
      return alert("Wrong answer try again");
    }
    let cur = new Date();
    let date = cur.getDate();
    let mnth = cur.getMonth();
    let year = cur.getFullYear();
    let min = cur.getMinutes();
    let time = new Date().getHours();
    let fulltime = time + ":" + min;
    let wre = counter.filter((vl) => vl > 0);
    let fin = "" + wre.length + "";
    let js = {
      id: "undefined",
      name: name,
      wrong: fin,
      date: date + "-" + months[mnth] + "-" + year,
      time: fulltime,
    };

    let { data: resp } = await axios.post(
      "https://guarded-crag-86799.herokuapp.com/newplayer",
      js
    );
    this.props.history.replace({
      pathname: "/report",
      state: { name: name, sel: sel },
    });
  };
  render() {
    let { newdata, name } = this.state;
    if (!name) {
      return <Redirect to="/start"></Redirect>;
    }
    console.log("wrong", this.state.wrong);
    return (
      <React.Fragment>
        <div
          className="container"
          style={{
            boxShadow:
              "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)",
            background: "#fefbd8",
          }}
        >
          <div className="row mt-5 header">
            <div className="col-12 text-center">
              <h3 style={{ color: "orange" }}>Welcome to the Brain Quiz</h3>
            </div>
          </div>
          <div className="row mt-4">
            <div className="col-6">
              <button
                className="btn btn-outline-danger"
                onClick={() => this.handleQuit()}
              >
                Quit Quiz
              </button>
            </div>
            <div className="col-6 text-right text-success">Welcome {name}</div>
          </div>
          <div className="row mt-5">
            <div className="col-12">
              {newdata
                ? newdata.map((vl, ind) =>
                    ind === this.state.curr ? (
                      <div className="row">
                        <span className="ml-2 mr-1 mt-5 col-md-1 col-1 ">
                          Q{this.state.curr + 1}
                          {">>"}
                        </span>{" "}
                        <span className="ml-1 mt-5 col-md-2 col-9">
                          Identify The{"  "}
                          <span className="d-md-none d-sm-block d-xs-block">
                            Image?
                          </span>
                        </span>
                        <img
                          src={vl.img}
                          className="col-md-4 col-11"
                          style={{
                            width: "200px",
                            height: "300px",
                            objectFit: "contain",
                          }}
                        ></img>
                        <span
                          style={{ fontSize: "18px", color: "black" }}
                          className=" col-md-1 col-2 mr-4  mt-5 d-none d-md-block"
                        >
                          Image?
                        </span>
                      </div>
                    ) : (
                      ""
                    )
                  )
                : ""}
            </div>
          </div>
          <div className="row mt-5">
            <div className="col-12">
              <ol>
                {newdata
                  ? newdata[this.state.curr].options.map((vl, ind) => (
                      <li
                        className={
                          this.state.sel[this.state.curr] === ind
                            ? "mt-3 listsel p-2"
                            : "mt-3 list p-2"
                        }
                        onClick={() => this.hanldeSel(ind)}
                        style={{
                          fontSize: "14px",
                          fontWeight: "500",
                          outline: "none",
                        }}
                      >
                        {vl}
                      </li>
                    ))
                  : ""}
              </ol>
            </div>
            <div className="col-12">
              {this.state.curr === 0 && this.state.curr <= 5 ? (
                ""
              ) : (
                <button
                  className="btn btn-warning"
                  onClick={() => this.handlePage(-1)}
                >
                  Prev
                </button>
              )}
              {this.state.curr === 4 ? (
                <button
                  className="btn btn-warning float-right mb-2 mt-1"
                  onClick={() => this.handleSubmit()}
                >
                  Submit
                </button>
              ) : (
                <button
                  className="btn btn-warning float-right mb-2 mt-1"
                  onClick={() => this.handlePage(1)}
                >
                  Next
                </button>
              )}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Second;
