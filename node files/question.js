let data = [
  {
    img: "https://i.ibb.co/QmctLR2/1200px-African-Bush-Elephant.jpg",
    options: ["Cat", "Dog", "Elephant", "Lion"],
    answer: "2",
  },
  {
    img: "https://i.ibb.co/0s7dcTS/aa79264f49aae4d4b2d77f0abdeb16fc.jpg",
    options: ["Cat", "Dog", "Elephant", "Lion"],
    answer: "3",
  },
  {
    img: "https://i.ibb.co/Fz02cwm/Getty-Images-962608834-fd496cfed51e4d2abe61c0af864fa681.jpg",
    options: ["Cat", "Dog", "Elephant", "Lion"],
    answer: "1",
  },
  {
    img: "https://i.ibb.co/R2Qy3GH/cow.jpg",
    options: ["Cat", "Dog", "Elephant", "Cow"],
    answer: "3",
  },
  {
    img: "https://i.ibb.co/DY4CmB8/i-Stock-177369626-1.jpgg",
    options: ["Goat", "Dog", "Elephant", "Cow"],
    answer: "0",
  },

  {
    img: "https://i.ibb.co/PWq6W5N/cat-759getty.jpg",
    options: ["Cat", "Dog", "Elephant", "Lion"],
    answer: "0",
  },
  {
    img: "https://i.ibb.co/5T6xZFp/download.jpg",
    options: ["Transvaal Lion", "African Lion", "Barbary Lion", "Cape Lion"],
    answer: "0",
  },
  {
    img: "https://i.ibb.co/6HT8st4/giraffe-gty-jt-201014-1602687277857-hp-Main-4x3t-992.jpg",
    options: ["Cat", "Giraffe", "Elephant", "Lion"],
    answer: "1",
  },
  {
    img: "https://i.ibb.co/hZ9cX4K/Polar-Bear-Alaska.jpg",
    options: ["Cat", "Dog", "Elephant", "Polar Bear"],
    answer: "3",
  },
  {
    img: "https://i.ibb.co/mcxTy6d/panda.jpg",
    options: ["Polar Bear", "Panda", "Brown Bear", "Pizzly"],
    answer: "1",
  },

  {
    img: "https://i.ibb.co/pL0r7ZN/horse.jpg",
    options: ["Cat", "Horse", "Elephant", "Lion"],
    answer: "1",
  },
  {
    img: "https://i.ibb.co/bNFQN5F/arabian.jpg",
    options: [
      "Arabian Horse",
      "American Horse",
      "Through Bread Horse",
      "Fresian Horse",
    ],
    answer: "0",
  },
  {
    img: "https://i.ibb.co/Mfk3mZX/tiger.jpg",
    options: ["Cat", "Tiger", "Elephant", "Lion"],
    answer: "1",
  },
  {
    img: "https://i.ibb.co/HGsNfjG/bengaltiger.jpg",
    options: [
      "Caspian Tiger",
      "Sumatran Tiger",
      "Royal Bengal Tiger",
      "Malayam Tiger",
    ],
    answer: "2",
  },
  {
    img: "https://i.ibb.co/tBv0xvD/220px-White-Persian-Cat.jpg",
    options: ["Ragdoll", "Persian Cat", "Siamese Cat", "Sphynx Cat"],
    answer: "1",
  },
  {
    img: "https://i.ibb.co/vXLVy7Y/Peacock-HP-691483428.jpg",
    options: ["Peacock", "Persian Cat", "Siamese Cat", "Sphynx Cat"],
    answer: "0",
  },
  {
    img: "https://i.ibb.co/NLp3FkC/F-103468.jpg",
    options: ["Peacock", "Rabbit", "Australian Rat", " Cat"],
    answer: "1",
  },
  {
    img: "https://i.ibb.co/sKXjBmZ/dear.png",
    options: ["Peacock", "Rabbit", "Deer", " Cat"],
    answer: "2",
  },
];

module.exports.data = data;
